#!/bin/bash
mvn clean package
STATUS=$?
if [ $STATUS -eq 0 ]; then
   mvn org.sonarsource.scanner.maven:sonar-maven-plugin:sonar        
echo "Deployment Successful"
ls

else
echo "Deployment Failed"
exit 1
fi
